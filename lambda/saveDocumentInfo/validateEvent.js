const validateEvent = event => event.every(({ value }) => {
	return !!value
})

module.exports = validateEvent