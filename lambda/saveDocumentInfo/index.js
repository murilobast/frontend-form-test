const validateEvent = require('./validateEvent')

exports.handler = (event, context, callback) => {
	const data = !!event
		? Object.keys(event).map(key => ({
			value: event[key],
			name: key
		}))
		: []

	const isValid = validateEvent(data)

	callback(null, {
			error: !isValid,
			data: {
					message: isValid ? 'Success' : 'Error',
					user: event
			}
	});
}