import { main, style } from '../assets.json'

export default markup => `
	<!DOCTYPE html>
	<html lang="pt-br">
		<head>
			<meta charset="utf-8" />
			<meta content="ie=edge" http-equiv="x-ua-compatible" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
			<link rel="stylesheet" href="${main.css}" />
			<link rel="stylesheet" href="${style.css}" />
		</head>
		<body>
			<main id="root">
				${markup}
			</main>
		</body>
	</html>
`.replace(/<svg\b[^<]*(?:(?!<\/svg>)<[^<]*)*<\/svg>/gi, '');