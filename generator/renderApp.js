import { Provider } from 'react-redux'
import { RouterContext } from 'react-router'
import { renderToString } from 'react-dom/server'

export default (store, renderProps) => renderToString(
	<Provider store={store}>
		<RouterContext { ...renderProps } />
	</Provider>
)