import { main, style } from '../assets.json'

export default (markup, state, criticalCss) => `
	<!DOCTYPE html>
	<html lang="pt-br">
		<head>
			<meta charset="utf-8" />
			<meta content="ie=edge" http-equiv="x-ua-compatible" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
			<link rel="shortcut icon" href="/favicon.png" />
			<title>GERU</title>
			<style>
				${criticalCss}
			</style>
			<link rel="preload" href="${main.css}" as="style" onload="this.onload=null;this.rel='stylesheet'">
			<link rel="preload" href="${style.css}" as="style" onload="this.onload=null;this.rel='stylesheet'">
			<noscript>
				<link rel="stylesheet" href="${main.css}">
				<link rel="stylesheet" href="${style.css}">
			</noscript>
  		<script>!function(n){"use strict";n.loadCSS||(n.loadCSS=function(){});var o=loadCSS.relpreload={};if(o.support=function(){var e;try{e=n.document.createElement("link").relList.supports("preload")}catch(t){e=!1}return function(){return e}}(),o.bindMediaToggle=function(t){var e=t.media||"all";function a(){t.media=e}t.addEventListener?t.addEventListener("load",a):t.attachEvent&&t.attachEvent("onload",a),setTimeout(function(){t.rel="stylesheet",t.media="only x"}),setTimeout(a,3e3)},o.poly=function(){if(!o.support())for(var t=n.document.getElementsByTagName("link"),e=0;e<t.length;e++){var a=t[e];"preload"!==a.rel||"style"!==a.getAttribute("as")||a.getAttribute("data-loadcss")||(a.setAttribute("data-loadcss",!0),o.bindMediaToggle(a))}},!o.support()){o.poly();var t=n.setInterval(o.poly,500);n.addEventListener?n.addEventListener("load",function(){o.poly(),n.clearInterval(t)}):n.attachEvent&&n.attachEvent("onload",function(){o.poly(),n.clearInterval(t)})}"undefined"!=typeof exports?exports.loadCSS=loadCSS:n.loadCSS=loadCSS}("undefined"!=typeof global?global:this);</script>
		</head>
		<body>
			<div id="root">
				${markup}
			</div>
			<script defer type="text/javascript" src="${main.js}"></script>
			<script>
				window.__STATE__ = ${JSON.stringify(state).replace(/</g, '\\u003c')};
			</script>
		</body>
	</html>
`.replace(/\n|\t/g, ' ').replace(/> *</g, '><')