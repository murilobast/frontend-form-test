const { main } = require('../assets.json')
const critical = require('critical')

const criticalResolver = markup =>
	critical.generate({
		inline: false,
		base: './dist/',
		folder: './',
		// css: [`./dist/${main.css}`],
		html: markup,
		width: 1300,
		height: 900,
		minify: true
	})

module.exports = criticalResolver