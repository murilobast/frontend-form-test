import { match } from 'react-router'
import serialize from 'serialize-javascript'

// Local
import template from './template'
import routes from '../src/routes'
import renderApp from './renderApp'
import basicTemplate from './basicTemplate'
import configureStore from '../src/configureStore'

const render = ({ path, criticalResolver }, callback) => {

	match({ routes, location: path }, (err, _, renderProps) => {
		const store = configureStore()
		let markup = ''
		renderApp(store, renderProps)

		if (err) {
			console.error(err.message)
			return
		}

		Promise.all(store.getState().promise).then(() => {
				// There's no need for promises in the initial state
				store.getState().promise = []
				const state = store.getState()
				const serialized = serialize(state, { isJSON: true })

				if (!renderProps) {
					console.error('Page Not Found!', path)
					return
				}

				markup = renderApp(store, renderProps)

				const markupWithBasicTemplate = basicTemplate(markup)
				criticalResolver(markupWithBasicTemplate).then(criticalCss => {
					const markupWithTemplate = template(markup, serialized, criticalCss)
					console.log('Generating HTML file for route', path)
					callback(null, markupWithTemplate)
				})
			}
		).catch(console.error)
	})
}

module.exports = render