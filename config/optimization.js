const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')

const minimizers = [
	new UglifyJsPlugin({
		cache: true,
		parallel: true,
		sourceMap: false
	}),
	new OptimizeCSSAssetsPlugin({})
]

module.exports = isDev => ({
	minimizer: isDev ? [] : minimizers ,
	splitChunks: isDev ? {} : {
		chunks: 'async',
		minSize: 30000,
		minChunks: 1,
		maxAsyncRequests: 5,
		maxInitialRequests: 3,
		name: true,
		cacheGroups: {
			default: {
				minChunks: 2,
				priority: -20,
				reuseExistingChunk: true
			},
			vendors: {
				test: /[\\/]node_modules[\\/]/,
				priority: -10
			},
			styles: {
				name: 'style',
				test: /\.styl$/,
				// chunks: 'all',
				enforce: true
			}
		}
	}
})