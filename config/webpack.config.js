const path = require('path')
const webpack = require('webpack')
const AssetsPlugin = require('assets-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')

// Loaders
const cssLoader = require('./loaders/cssLoader')

// Plugins
const htmlPlugin = require('./plugins/htmlPlugin')
const miniCssPlugin = require('./plugins/miniCssPlugin')

// Optimization
const optimization = require('./optimization')

module.exports = (env, argv) => {
	const isDev = argv.mode !== 'production'

	return {
		module: {
			rules: [
				{
					test: /\.(png|jpg|svg|gif)$/,
					loader: 'url-loader',
					query: {
						limit: 1000,
						name: 'static/media/[name].[ext]'
					}
				},
				{
					test: /\.js$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader'
					}
				},
				...cssLoader(isDev)
			]
		},
		plugins: [
			new CleanWebpackPlugin(['dist'], {
				root: path.resolve(__dirname, '../'),
				verbose: true
			}),
			new AssetsPlugin({
				filename: 'assets.json'
			}),
			...htmlPlugin(),
			...miniCssPlugin(isDev),
			new webpack.ProvidePlugin({
				React: 'react'
			})
		],
		output: {
			filename: '[name]-[hash:8].bundle.js',
			publicPath: '/'
		},
		optimization: optimization(isDev),
		resolve: {
			modules: [
				path.resolve(__dirname, '../src/'),
				'node_modules'
			]
		},
		devServer: {
			historyApiFallback: true
		}
	}
}