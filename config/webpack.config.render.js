const path = require('path')
const webpack = require('webpack')
const StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin')

// Local
const routes = require('../src/constants/routes')
const criticalResolver = require('../generator/criticalResolver')

const routesArray = Object
	.keys(routes)
	.map(key => routes[key])

module.exports ={
	entry: path.resolve(__dirname, '../generator/index.js'),
	output: {
		filename: 'generator.js',
		path: path.resolve(__dirname, '../dist/'),
		libraryTarget: 'umd'
	},
	target: 'node',
	module: {
		rules:[
			{
				exclude: [
					/\.(js|jsx)$/,
					/\.json$/,
					/\.(svg|png|jpg)$/,
					/\.inline\.svg$/
				],
				loader: 'ignore-loader',
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: [{
					loader: 'babel-loader'
				}],
			},
			{
				exclude: /\.inline\.svg$/,
				test: /\.(svg|png|jpg)$/,
				loader: 'url-loader',
				query: {
					limit: 1000,
					name: `/static/media/[name].[ext]`
				}
			}
		]
	},
	plugins: [
		new webpack.optimize.LimitChunkCountPlugin({
			maxChunks: 1,
		}),
		new webpack.ProvidePlugin({
			React: 'react'
		}),
		new StaticSiteGeneratorPlugin({
			locals: { criticalResolver },
			paths: routesArray.map(({ route }) => route)
		})
	],
	resolve: {
		modules: [
			path.resolve(__dirname, '../src/'),
			'node_modules'
		]
	}
}