import { Fragment } from 'react'
// Components
import Header from 'components/shared/Header'

// Stylus
import 'styles/main.styl'

const App = ({ children }) => (
	<Fragment>
		<Header />
		{children}
	</Fragment>
)

export default App