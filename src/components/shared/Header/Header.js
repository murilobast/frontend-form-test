import { Link } from 'react-router'
import classNames from 'classnames'

// Assets
import logoImage from 'assets/logo.svg'

// Components
import Menu from './Menu'
import Wrapper from 'components/shared/Wrapper'

// Enhancers
import menuEnhancer from 'enhancers/menu'

const Header = ({ isOpen, menuHandler }) => (
	<header className={classNames(
		'header',
		{ 'is--open': isOpen }
	)}>
		<div className="header__mask" />
		<div className="header__content">
			<Wrapper>
				<div className="header__logo">
					<Link to="/">
						<img src={logoImage} alt="Geru" />
					</Link>
				</div>
				<Menu
					isOpen={isOpen}
					menuHandler={menuHandler}
				/>
			</Wrapper>
		</div>
	</header>
)

export default menuEnhancer(Header)
