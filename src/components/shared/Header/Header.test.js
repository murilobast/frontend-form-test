import { mount } from 'enzyme'
import { Link } from 'react-router'

// Constants
import routes from 'constants/routes'

// Components
import Header from './Header'

const { home } = routes

describe('Header component', () => {
	let wrapper = null

	beforeEach(() => {
		wrapper = mount(<Header />)
	})

	it('Has a Logo', () => {
		const logoElement = wrapper.find('.header__logo img')
		expect(logoElement.length).toEqual(1)
	})
	
	it('Logo has a link to home path', () => {
		const logoLinkElement = wrapper.find('.header__logo').find(Link)
		const path = logoLinkElement.props().to
		expect(path).toEqual(home.route)
	})

	it('Renders with links', () => {
		const linkElements = wrapper.find('.header__links').find(Link)
		expect(linkElements.length > 1).toEqual(true)
	})

	it('Shows menu when menu button is clicked', () => {
		const openClass = 'is--open'
		// Click on menu button once to show menu
		wrapper.find('.header__menu').simulate('click')
		expect(wrapper.find('.header').hasClass(openClass)).toBe(true)

		// Click on menu button again to hide menu
		wrapper.find('.header__menu').simulate('click')
		expect(wrapper.find('.header').hasClass(openClass)).toBe(false)
	})
})