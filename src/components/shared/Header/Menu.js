import { Fragment } from 'react'
import { Link } from 'react-router'

const Menu = ({ isOpen, menuHandler }) => (
	<Fragment>
		<ul className="header__links">
			<li className="header__link" role="button" onClick={menuHandler}>
				<Link to="#">Como funciona</Link>
			</li>
			<li className="header__link" role="button" onClick={menuHandler}>
				<Link to="#">Privacidade</Link>
			</li>
			<li className="header__link" role="button" onClick={menuHandler}>
				<Link to="#">Ajuda</Link>
			</li>
		</ul>
		<button
			className="header__menu"
			onClick={menuHandler}
		>
			menu
		</button>
		</Fragment>
)

export default Menu
