import classNames from 'classnames'

const Wrapper = ({ small = false, smaller = false, children }) => (
	<div className={classNames(
		'container',
		{ 'container--small': small },	
		{ 'container--smaller': smaller }	
	)}>
		{children}
	</div>
)

export default Wrapper