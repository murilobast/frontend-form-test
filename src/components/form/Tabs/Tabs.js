import classNames from 'classnames'

// Components
import Wrapper from 'components/shared/Wrapper'

const renderTabs = (tabs, current) => tabs.map(({ name, key }, i) => (
	<li
		key={key}
		className={classNames(
			'tabs__item',
			{ 'tabs__item--filled': i < current },
			{ 'tabs__item--active': i === current }
		)}
	>
		<span className="tabs__number">
			{i + 1}
		</span>
		<span className="tabs__text">
			{name}
		</span>
	</li>
))

const Tabs = ({ current = 0, tabs }) => (
	<section className="tabs">
		<Wrapper small>
			<ul className="tabs__list">
				{renderTabs(tabs, current)}
			</ul>
		</Wrapper>
	</section>
)

export default Tabs