import { mount } from 'enzyme'

// Components
import Tabs from './Tabs'

// Constants
import formSteps from 'constants/formSteps'

describe('Tabs component', () => {
	let wrapper = null

	beforeEach(() => {
		wrapper = mount(<Tabs tabs={formSteps} />)
	})

	it('Renders all provided tabs', () => {
		expect(wrapper.find('.tabs__item').length).toEqual(formSteps.length)
	})

	it('Renders all provided information', () => {
		formSteps.forEach(({ name }, i) => {
			expect(wrapper.find('.tabs__text').at(i).text()).toEqual(name)
		})
	})
	it('Set correct classes to tabs when current value is passed', () => {
		const current = 2
		wrapper.setProps({ current })
		expect(wrapper.find('.tabs__item').at(current).hasClass('tabs__item--active')).toBe(true)
		expect(wrapper.find('.tabs__item').at(current - 1).hasClass('tabs__item--active')).toBe(false)
		expect(wrapper.find('.tabs__item').at(current - 1).hasClass('tabs__item--filled')).toBe(true)
		expect(wrapper.find('.tabs__item').at(current + 1).hasClass('tabs__item--filled')).toBe(false)
	})
})