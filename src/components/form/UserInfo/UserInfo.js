// Comonents
import Wrapper from 'components/shared/Wrapper'

const UserInfo = ({
	cpf,
	name,
	loanAmout,
	loanReason,
	loanInstallments
}) => (
	<section className="user">
		<Wrapper small>
			<div className="user__info">
				<div className="user__group">
					<h3 className="user__label">Me chamo:</h3>
					<p className="user__text user__text--name">{name}</p>
				</div>
				<div className="user__group user__group--small">
					<h3 className="user__label">CPF:</h3>
					<p className="user__text user__text--cpf">{cpf}</p>
				</div>
			</div>
			<div className="user__loan">
				<div className="user__group">
					<h3 className="user__label">Preciso de:</h3>
					<p className="user__text user__text--loanAmout">{loanAmout}</p>
				</div>
				<div className="user__group">
					<h3 className="user__label">Quero pagarm em:</h3>
					<p className="user__text user__text--loanInstallments">{loanInstallments}</p>
				</div>
				<div className="user__group">
					<h3 className="user__label">Para:</h3>
					<p className="user__text user__text--loanReason">{loanReason}</p>
				</div>
			</div>
		</Wrapper>
	</section>
)

export default UserInfo