import { shallow } from 'enzyme'

// Components
import UserInfo from './UserInfo'

// Constants
import userMock from 'constants/userMock'

describe('UserInfo component', () => {
	it('Renders with provided information', () => {
		const wrapper = shallow(<UserInfo {...userMock} />)

		Object.keys(userMock).forEach(key => {
			expect(wrapper.find(`.user__text--${key}`).text()).toEqual(`${userMock[key]}`)
		})
	})
})