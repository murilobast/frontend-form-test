import { mount } from 'enzyme'

// Components
import Input, { Input as CleanInput } from './Input'

// Validators
import { rg as rgValidator } from 'helpers/validators'

describe('Input component', () => {
	let wrapper = null

	beforeEach(() => {
		wrapper = mount(<Input
			type="number"
			validator={rgValidator}
		/>)
	})
	it('Renders with provided props', () => {
		const detectedType = wrapper
			.find('input')
			.getDOMNode().attributes
			.getNamedItem('type').value
		expect(detectedType).toBe('number')
	})

	it('Changes it\'s value and validate the inputed value', () => {
		expect(wrapper.find(CleanInput).prop('valid')).toBe(false)
		wrapper.find('input').simulate('change', {
			target: { value: '48.861.836-8' }
		})
		expect(wrapper.find('.input').hasClass('input--invalid')).toBe(false)
		expect(wrapper.find(CleanInput).prop('valid')).toBe(true)
		wrapper.find('input').simulate('change', {
			target: { value: '48.861.83' }
		})
		expect(wrapper.find('.input').hasClass('input--invalid')).toBe(true)
		expect(wrapper.find(CleanInput).prop('valid')).toBe(false)
	})

	it('Calls update updateFormState onChange', () => {
		const updateFormState = jest.fn()
		wrapper.setProps({ updateFormState })
		expect(updateFormState).toHaveBeenCalledTimes(0)
		wrapper.find('input').simulate('change', {
			target: { value: '48.861.836-8' }
		})
		expect(updateFormState).toHaveBeenCalled()
	})
})