import MaskedInput from 'react-text-mask'

const getProps = ({ name, type, value, onBlur, onFocus, onChange }) => ({
	id: name,
	type: type,
	value: value,
	onBlur: onBlur,
	onFocus: onFocus,
	onChange: onChange,
	className: 'input__regular'
})

const RegularInput = ({ mask, ...props }) => {
	const inputProps = getProps(props)
	return !!mask
		? <MaskedInput guide={false} mask={mask} {...inputProps} />
		: <input {...inputProps} />
}

export default RegularInput