// Enhancers
import selectEnhancer from 'enhancers/select'

const getOptions = options => options.map(({ name, value }) => (
	<option
		key={value}
		value={value}
		// selected={value === 'test2'}
		className="select__option"
	>
		{name}
	</option>
))

const SelectInput = ({ name, value, options, onChange, isLoading, onFocus, onBlur }) => (
	<select
		id={name}
		value={value}
		onBlur={onBlur}
		onFocus={onFocus}
		onChange={onChange}
		disabled={isLoading}
		className="input__select"
	>
		<option value="" className="select__option">
			Selecione
		</option>
		{getOptions(options)}
	</select>
)

export default selectEnhancer(SelectInput)