import classNames from 'classnames'

// Components
import RadioInput from './RadioInput'
import SelectInput from './SelectInput'
import RegularInput from './RegularInput'

// Enhancers
import inputEnhancer from 'enhancers/input'

const inputs = {
	'radio': RadioInput,
	'select': SelectInput
}

const getClassNames = (type, { focused, valid, touched }) => classNames(
	'input',
	`input--${type}`,
	{ 'input--focused': focused },
	{ 'input--invalid': !valid && !focused && touched }
)

const getInputType = (type, props) => {
	const CurrentInput = inputs[type] || RegularInput
	return <CurrentInput {...props} />
}

export const Input = ({ name, label, type = 'text', ...props }) => (
	<div className={getClassNames(type, props)}>
		<label
			htmlFor={name}
			className="input__label"
		>
			{label}
		</label>
		{getInputType(type, { type, name, ...props })}
	</div>
)

export default inputEnhancer(Input)