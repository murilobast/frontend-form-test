import { Fragment } from 'react'
import classNames from 'classnames'

const getRadioButtons = ({
	values,
	onChange,
	name: parentName,
	value: parentValue
}) => values.map(({ value, name }) => (
	<label
		key={name}
		htmlFor={name}
		className={classNames(
			'input__radio',
			{ 'input__radio--active': value === parentValue }
		)}
	>
		<span className="input__radio-label" >{name}</span>
		<input
			id={name}
			type="radio"
			value={value}
			name={parentName}
			onChange={onChange}
			checked={value === parentValue}
		/>
	</label>
))

const RadioInput = props => (
	<Fragment>
		{getRadioButtons(props)}
	</Fragment>
)

export default RadioInput