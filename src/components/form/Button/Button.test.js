import { mount, shallow } from 'enzyme'

// Components
import Button from './Button'

describe('Button component', () => {
	it('Renders with provided props', () => {
		const wrapper = mount(<Button
			disabled={false}
			type="button"
			text="Hello World"
		/>)
		expect(wrapper.find('button').getDOMNode().disabled).toBe(false)
		wrapper.setProps({ disabled: true })
		expect(wrapper.find('button').getDOMNode().disabled).toBe(true)
		expect(wrapper.find('button').is('[type="button"]')).toBe(true)
		wrapper.setProps({ type: 'submit' })
		expect(wrapper.find('button').is('[type="submit"]')).toBe(true)
		expect(wrapper.find('button').text()).toBe('Hello World')
	})

	it('Calls onClick', () => {
		const clickHandler = jest.fn()
		const wrapper = shallow(<Button
			text="Hello World"
			onClick={clickHandler}
		/>)
		expect(clickHandler).toHaveBeenCalledTimes(0)
		wrapper.find('button').simulate('click')
		expect(clickHandler).toHaveBeenCalled()
	})
})