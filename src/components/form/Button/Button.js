const NOOP = () => {}

const Button = ({ text, type = "button", disabled, onClick = NOOP }) => (
	<button
		type={type}
		onClick={onClick}
		className="button"
		disabled={disabled}
	>
		{text}
	</button>
)

export default Button