import { mount } from 'enzyme'

// Components
import Form from './Form'

// Constants
import formFields from 'constants/formFields'

describe('Form component', () => {
	let wrapper = null
	beforeEach(() => {
		wrapper = mount(<Form
			title="Dados Pessoais"
			fieldGroups={formFields}
		/>)
	})
	
	it('Renders with provided props', () => {
		expect(wrapper.find('.form__title').text()).toBe('Dados Pessoais')
		expect(wrapper.find('.form__group')).toHaveLength(formFields.length)
		
		formFields.forEach((fieldSet, i) => {
			expect(wrapper.find('.form__group').at(i).find('.input')).toHaveLength(fieldSet.length)
		})
	})
})