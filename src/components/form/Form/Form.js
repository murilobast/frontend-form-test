// Components
import Input from 'components/form/Input'
import Button from 'components/form/Button'
import Wrapper from 'components/shared/Wrapper'

// Enhancers
import formEnhancer from 'enhancers/form'

const getFields = (fields, updateFormState) => fields.map(({ name, ...props }) => (
	<Input
		key={name}
		name={name}
		updateFormState={updateFormState}
		{...props}
	/>
))

const getFieldGroups = (fieldGroups, updateFormState) => fieldGroups.map((fields, i) => (
	<div className="form__group" key={`group-${i}`}>
		{getFields(fields, updateFormState)}
	</div>
))

const Form = ({ title, formIsValid, handleSubmit, fieldGroups, updateFormState }) => (
	<section className="form">
		<Wrapper smaller>
			<form className="form__form" onSubmit={handleSubmit}>
				<h2 className="form__title">
					{title}
				</h2>
					{getFieldGroups(fieldGroups, updateFormState)}
				<Button
					type="submit"
					text="Continuar"
					disabled={!formIsValid}
				/>
			</form>
		</Wrapper>
	</section>
)

export default formEnhancer(Form)