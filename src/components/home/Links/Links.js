import { Link } from 'react-router'

// Components
import Wrapper from 'components/shared/Wrapper'

const getLinks = links => links.map(({ name, path }, i) => (
	<Link
		to={path}
		key={`link-${i}`}
		className="links__item"
	>
		{name}
	</Link>
))

const Links = ({ links }) => (
	<div className="links">
		<Wrapper>
			{getLinks(links)}
		</Wrapper>
	</div>
)

export default Links