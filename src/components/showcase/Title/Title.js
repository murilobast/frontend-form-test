// Components
import Wrapper from 'components/shared/Wrapper'

const Title = ({ text }) => (
	<Wrapper>
		<h1 className="title">{text}</h1>
	</Wrapper>
)

export default Title