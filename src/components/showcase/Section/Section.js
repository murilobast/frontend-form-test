// Components
import Wrapper from 'components/shared/Wrapper'

const Section = ({ title, children }) => (
	<section className="showcase">
		<Wrapper>
			<h2 className="showcase__title">{title}</h2>
			{children}
		</Wrapper>
	</section>
)

export default Section