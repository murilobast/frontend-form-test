const dateValidator = date => {
	if (!/^\d{2}\/\d{2}\/\d{4}$/.test(date))
		return false
	const [day, month, yearString] = date.split('/')
	const year = Number(yearString) - 2000
	const currentDate = new Date()
	const currentDay = currentDate.getDate()
	const currentYear = currentDate.getYear() - 100
	const currentMonth = currentDate.getMonth() + 1
	const compareMonth = Number(month) < currentMonth
	const compareYear = Number(year) < currentYear
	const compareDate = compareYear
		? true
		: Number(year) === currentYear
			? compareMonth ? true : Number(day) < currentDay
			: false
	const isValid = Number(day) <= 31 && Number(month) <= 12 && compareDate
	return isValid
}

export default dateValidator