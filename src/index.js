import { render } from 'react-dom'
import { match, browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

// Configs
import routes from 'routes'
import configureStore from 'configureStore'

// Containers
import Root from './containers/Root'

// Helpers
import isDev from 'helpers/isDev'
import fontLoader from 'helpers/FontLoader'

const { pathname, search, hash } = window.location
const location = `${pathname}${search}${hash}`
const store = configureStore()
const history = syncHistoryWithStore(browserHistory, store)
const rootProps = { store, history }
const rootEl = document.getElementById('root')

const renderApp = rootProps => (
	match({ routes, location }, (err, redirectLocation, renderProps) => {
		const props = {
			...rootProps,
			...renderProps
		}
		render(
			<Root {...props} />,
			rootEl
		)
	})
)

renderApp(rootProps)

fontLoader([
	{ name: 'Montserrat', weights: [400, 600, 700, 800] }
])

if (isDev && module.hot) {
	module.hot.accept('containers/Root', () => renderApp(rootProps))
	module.hot.accept('routes', () => renderApp(rootProps))
}