import axios from 'axios'
import { compose, withState, withHandlers, lifecycle } from 'recompose'


const handleResponseData = data => data.splice(1, data.length).map(({ value }) => ({
	value,
	name: value
}))

export default compose(
	withState('options', 'setOptions', []),
	withState('isLoading', 'setIsLoading', false),
	withHandlers({
		fetchOptionsFromAPI: ({ ajax, setOptions, setIsLoading }) => () => {
			setIsLoading(true)
			const fetchPromise = axios(ajax)
				.then(({ status, data }) => {
					setIsLoading(false)
					if (status === 200)
						setOptions(handleResponseData(data.data))
				})
		}
	}),
	lifecycle({
		componentDidMount() {
			const { fetchOptionsFromAPI } = this.props
			fetchOptionsFromAPI()
		}
	})
)