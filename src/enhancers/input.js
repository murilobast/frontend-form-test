import { compose, withState, withHandlers } from 'recompose'

export default compose(
	withState('value', 'setValue', ''),
	withState('valid', 'setValid', false),
	withState('touched', 'setTouched', false),
	withState('focused', 'setFocused', false),
	withHandlers({
		onChange: ({
			name,
			validator,
			setValue,
			setValid,
			setTouched,
			updateFormState = () => {}
		}) => ({ target }) => {
			const { value } = target
			const valid = validator(value)
			setValue(value)
			setTouched(true)
			setValid(valid)
			updateFormState(name, {
				value, valid
			})
		},
		onBlur: ({ setFocused }) => () => setFocused(false),
		onFocus: ({ setFocused }) => () => setFocused(true)
	})
)