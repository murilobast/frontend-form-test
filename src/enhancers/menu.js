import { compose, withState, withHandlers } from 'recompose'

export default compose(
	withState('isOpen', 'setIsOpen', false),
	withHandlers({
		menuHandler: ({ isOpen, setIsOpen }) => () => setIsOpen(!isOpen)
	})
)
