import axios from 'axios'
import { compose, withState, withHandlers } from 'recompose'

// Constants
import formFields from 'constants/formFields'

const POST_URL = 'https://ib4u23hzek.execute-api.sa-east-1.amazonaws.com/staging/save-document-info'

const getInitialFormDataState = ({ fieldGroups }) => {
	return fieldGroups
		.reduce((array, current) => ([
			...array,
			...current
		]), [])
		.reduce((object, { name }) => ({
			...object,
			[name]: {
				value: '',
				valid: false
			}
		}), {})
}
export default compose(
	withState('fieldGroups', 'setFieldGroups', ({ fieldGroups }) => fieldGroups),
	withState('formIsValid', 'setFormIsValid', false),
	withState('formData', 'setFormData', getInitialFormDataState),
	withHandlers({
		updateFormState: ({ formData, setFormData, setFormIsValid }) => (key, value) => {
			const updatedFormData = {
				...formData,
				[key]: value
			}
			setFormData(updatedFormData)
			const formIsValid = Object.keys(formData)
				.map(key => updatedFormData[key].valid)
				.every(valid => valid)
			setFormIsValid(formIsValid)
		},
		handleSubmit: ({ formIsValid, formData, setFieldGroups, setFormIsValid, setFormData }) => e => {
			e.preventDefault()
			if (formIsValid) {
				const data = Object.keys(formData).reduce((object, key) => {
					return {
						...object,
						[key]: formData[key].value
					}
				}, {})
				axios(POST_URL, { method: 'POST', data }).then(response => {
					confirm('Dados enviados com sucesso!')
					setFieldGroups([])
					setFieldGroups(formFields)
					setFormData(getInitialFormDataState({ fieldGroups: formFields }))
					setFormIsValid(false)
				})
			}
		}
	})
)