import { Route, IndexRoute } from 'react-router'

// Containers
import App from './containers/App'

// Constants
import { form, showcase } from 'constants/routes'

const loadRoute = callback => (module) => {
	if (!module) {
		callback()
	} else {
		callback(null, module.default)
	}
}

const errorOnLoadingRoute = error => {
	console.log(error)
	throw new Error('Error on dynamic route loading', error)
}

const routes = (
	<Route
		path={'/'}
		component={App}
	>
		<IndexRoute
			getComponent={(location, cb) => {
				import(/* webpackChunkName: "home" */ './pages/Home')
					.then(loadRoute(cb, null))
					.catch(errorOnLoadingRoute)
			}}
		/>
		<Route
			path={form.route}
			getComponent={(location, cb) => {
				import(/* webpackChunkName: "form" */ './pages/Form')
					.then(loadRoute(cb, null))
					.catch(errorOnLoadingRoute)
			}}
		/>
		<Route
			path={showcase.route}
			getComponent={(location, cb) => {
				import(/* webpackChunkName: "form" */ './pages/Showcase')
					.then(loadRoute(cb, null))
					.catch(errorOnLoadingRoute)
			}}
		/>
	</Route>
)

export default routes