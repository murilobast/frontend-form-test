import { Fragment } from 'react'

// Components
import Tabs from 'components/form/Tabs'
import Form from 'components/form/Form'
import UserInfo from 'components/form/UserInfo'

// Constants
import userMock from 'constants/userMock'
import formSteps from 'constants/formSteps'
import formFields from 'constants/formFields'

const FormPage = () => (
	<Fragment>
		<UserInfo {...userMock} />
		<Tabs
			current={1}
			tabs={formSteps}
		/>
		<Form
			title="Dados Pessoais"
			fieldGroups={formFields}
		/>
	</Fragment>
)

export default FormPage