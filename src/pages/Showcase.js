import { Fragment } from 'react'

// Helpers
import { rg as rgValidator } from 'helpers/validators'

// Components
import Tabs from 'components/form/Tabs'
import Input from 'components/form/Input'
import Button from 'components/form/Button'
import Title from 'components/showcase/Title'
import Section from 'components/showcase/Section'
import UserInfo from 'components/form/UserInfo'

// Constants
import userMock from 'constants/userMock'
import formSteps from 'constants/formSteps'

const Showcase = () => (
	<Fragment>
		<Title text="Showcase" />
		<Section title="Regular Button">
			<Button text="Button text" />
		</Section>
		<Section title="Disabled Button">
			<Button text="Button text" disabled />
		</Section>
		<Section title="User Info">
			<UserInfo {...userMock} />
		</Section>
		<Section title="Form Tabs">
			<Tabs
				current={1}
				tabs={formSteps}
			/>
		</Section>
		<Section title="Regular Input">
			<Input
				type="text"
				label="Númenro do RG"
				validator={rgValidator}
				mask={value => value.length > 10
					? [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/]
					: [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/]
				}
			/>
		</Section>
		<Section title="Select Input with ajax">
			<Input
				type="select"
				label="Orgão emissor"
				validator={value => !!value}
				ajax="https://ib4u23hzek.execute-api.sa-east-1.amazonaws.com/staging/issuer-organs"
			/>
		</Section>
		<Section title="Radio Input">
			<Input
				type="radio"
				label="Sexo"
				validator={value => !!value}
				values={[
					{ name: 'Feminino', value: 'F' },
					{ name: 'Masculino', value: 'M' }
				]}
			/>
		</Section>
	</Fragment>
)

export default Showcase