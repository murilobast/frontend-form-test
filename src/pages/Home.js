import { Fragment } from 'react'

// Components
import Links from 'components/home/Links'
import Title from 'components/showcase/Title'

const Home = () => (
	<Fragment>
		<Title text="Welcome!" />
		<Links links={[
			{ name: 'Go to the Test', path: '/form' },
			{ name: 'Showcase', path: '/showcase' }
		]} />
	</Fragment>
)

export default Home