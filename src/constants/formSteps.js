const formSteps = [
	{ key: 'simulation', name: 'Simule' },
	{ key: 'fill', name: 'Preencha o cadastro' },
	{ key: 'review', name: 'Revise seu pedido' },
	{ key: 'final', name: 'Finalize o pedido' },
]

export default formSteps