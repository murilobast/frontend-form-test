const routes = {
	home: {
		route: '/',
		name: 'Home'
	},
	form: {
		route: '/form',
		name: 'Form'
	},
	showcase: {
		route: '/showcase',
		name: 'Showcase'
	}
}

module.exports = routes