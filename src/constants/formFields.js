// Validators
import {
	rg as rgValidator,
	date as dateValidator
} from 'helpers/validators'

const defaultValidator = value => !!value

const formFields = [
	[
		{
			name: 'rg',
			label: 'Número do RG',
			validator: rgValidator,
			mask: value => value.length > 10
				? [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/]
				: [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/]
		},
		{
			name: 'date-of-issue',
			label: 'Data de emissão',
			validator: dateValidator,
			mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
		},
		{
			type: 'select',
			name: 'issuer-organ',
			label: 'Orgão emissor',
			validator: defaultValidator,
			ajax: 'https://ib4u23hzek.execute-api.sa-east-1.amazonaws.com/staging/issuer-organs'
		}
	],
	[
		{
			name: 'gender',
			label: 'Sexo',
			type: 'radio',
			values: [
				{ name: 'Feminino', value: 'F' },
				{ name: 'Masculino', value: 'M' }
			],
			validator: defaultValidator
		}
	]
]

export default formFields