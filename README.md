# Frontend Test

[Live Demo](https://d6p0qpz5qwtk8.cloudfront.net)

### Tech

* React 16
* Recompose
* Webpack 4
* Stylus
* Lambda Functions
* Jest
* Enzyme
* Static HTML rendering
* Axios

### Requirements
NodeJS 7.10+

### Installation
With Yarn
```sh
$ yarn install
```

With NPM
```sh
$ npm i
```

### Running
Development enviroment

```sh
$ yarn start
```

### Production
Run the following command to generate all bundles and static files to ./dist/ folder
```sh
$ yarn build
```
Then just run any static file server (http-server, apache, etc) on ./dist/ folder

### Tests
You can run all tests by running:
```sh
$ yarn test
```
You can also let Jest watch your files while developing:
```sh
$ yarn test --watch
```


#### Thanks :)